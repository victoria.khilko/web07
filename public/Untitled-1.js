window.addEventListener("DOMContentLoaded", function () {
    const Bulbul = 500;
    const Milk_Ulun = 600;
    const La_manana = 700;
    const EXTRA_PRICE = 0;
    const No_Adicion = 0;
    const Menta = 50;
    const Fresa = 100;
    const Un_regalo = 100;
    let price = Bulbul;
    let extraPrice = EXTRA_PRICE;
    let result;
    let calc = document.getElementById("calc");
    let myNum = document.getElementById("number");
    let resultSpan = document.getElementById("result-span");
    let select = document.getElementById("select");
    let radiosDiv = document.getElementById("radio-div");
    let radios = document.querySelectorAll("#radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("check-div");
    let checkbox = document.getElementById("checkbox");
    select.addEventListener("change", function (event) {
        let option = event.target;
        if (option.value === "1") {
            radiosDiv.classList.add("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = Bulbul;
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = No_Adicion;
            price = Milk_Ulun;
            document.getElementById("radio1").checked = true;
        }
        if (option.value === "3") {
            checkboxDiv.classList.remove("d-none");
            radiosDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = La_manana;
            checkbox.checked = false;
        }
    });
    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {
            let radio = event.target;
            if (radio.value === "No_Adicion") {
                extraPrice = No_Adicion;
            }
            if (radio.value === "Menta") {
                extraPrice = Menta;
            }
            if (radio.value === "Fresa") {
                extraPrice = Fresa;
            }
        });
    });
    checkbox.addEventListener("change", function () {
        if (checkbox.checked) {
            extraPrice = Un_regalo;
        } else {
            extraPrice = EXTRA_PRICE;
        }
    });
    calc.addEventListener("change", function () {
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        result = price * myNum.value + extraPrice * myNum.value;
        resultSpan.innerHTML = result;
    });
});