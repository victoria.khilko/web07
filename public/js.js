jQuery(document).ready(function($) {
    $(".sl").slick({
     dots: true,
     infinite: true,
     speed: 500,
     slidesToShow: 4,
     slidesToScroll: 1,
     responsive: [
          {
             breakpoint: 500, // размер экрана 500
            settings: {
               slidesToShow: 2, // тут меняем slidesToShow
               slidesToScroll: 1
             }
           }
     ]
   });
  });